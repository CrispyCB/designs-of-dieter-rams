const ImageOpacity = (list, mainImage, opacity) => (e) => {
  list.forEach((img) => (img.style.opacity = 1));
  mainImage.classList.add("fade-out");
  setTimeout(() => mainImage.classList.remove("fade-out"), 500);
  mainImage.src = e.target.src;
  mainImage.classList.add("fade-in");
  setTimeout(() => mainImage.classList.remove("fade-in"), 500);
  e.target.style.opacity = opacity;
  e.target.classList.add("border-blue");
  setTimeout(() => e.target.classList.remove("border-blue"), 500);
};

function Title() {
  const title = document.createElement("h2");
  title.textContent = "Selected Designs";
  title.classList.add("page-title", "main-font", "primary-color");
  return title;
}

function MainImage(defaultImage) {
  const mainImage = document.createElement("img");
  mainImage.id = "main-image";
  mainImage.src = defaultImage;
  mainImage.classList.add("border-black");
  return mainImage;
}

function ImageList(opacity, imageContainer) {
  const imageList = [
    "https://dieter-rams-selected-designs.s3.amazonaws.com/606-Universal-Shelving-System.jpg",
    "https://dieter-rams-selected-designs.s3.amazonaws.com/Vitsoe_620_Chair_Programme.jpg",
    "https://dieter-rams-selected-designs.s3.amazonaws.com/SK554.jpg",
    "https://dieter-rams-selected-designs.s3.amazonaws.com/Nizo-1000-Camera.jpg",
  ];
  const imageListNodes = [];
  const imageListContainer = document.createElement("section");
  imageListContainer.id = "image-list-container";
  imageList.forEach((image) => {
    let clickableImage = document.createElement("img");
    clickableImage.src = image;
    clickableImage.classList.add("thumbnail", "border-black");
    imageListNodes.push(clickableImage);
    clickableImage.addEventListener(
      "click",
      ImageOpacity(imageListNodes, imageContainer, opacity)
    );
    imageListContainer.append(...imageListNodes);
  });
  return imageListContainer;
}

function loadPage() {
  const opacity = 0.6;
  const container = document.getElementById("designs");
  const mainImage = MainImage(
    "https://dieter-rams-selected-designs.s3.amazonaws.com/606-Universal-Shelving-System.jpg"
  );
  container.classList.add("page", "flex", "even-spacing");
  container.append(Title(), mainImage, ImageList(opacity, mainImage));
}
loadPage();
