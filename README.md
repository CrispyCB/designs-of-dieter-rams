# The Designs of Dieter Rams

My version of the tribute site project for FreeCodeCamp, centering around the industrial designer Dieter Rams. Built as a single-page site using HTML, CSS and JavaScript, deployed with GitLab CI and hosted on Amazon S3.
